from rest_framework import serializers
from . import models

class PlaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Place
        fields = '__all__'

class MenuSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Menu
        fields = '__all__'

class SectionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Sections
        fields = '__all__'

class IngredientsSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Ingredients
        fields = '__all__'

class ContentsSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Contents
        fields = '__all__'

class TablesSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Tables
        fields = '__all__'

class WaitersSerializer(serializers.ModelSerializer):
    class Meta:
       model = models.Waiters
       fields = '__all__'

class OrdersSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Orders
        fields = '__all__'

class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Country
        fields = '__all__'

class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.City
        fields = '__all__'
        
class Orders_itemSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Orders_item
        fields = '__all__'
        
