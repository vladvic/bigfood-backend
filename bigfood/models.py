from django.db import models
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


class Place(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    telephone = models.CharField(max_length=100)
    email = models.EmailField()
    city = models.ForeignKey('City', on_delete=models.CASCADE)

    def __str__(self):
        return '%s, %s, %s, %s, %s' % (self.name, self.address, self.telephone, self.email, self.city)

class Menu(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    photo = models.ImageField(upload_to=None, height_field=None, width_field=None, max_length=100)
    contentes = models.ForeignKey('Contents', on_delete=models.CASCADE) 
    description = models.CharField(max_length=100)
    price = models.FloatField(null=True)
    calories = models.IntegerField(null=True)
    time = models.IntegerField(null=True)
    section_id = models.ForeignKey('Sections', on_delete=models.CASCADE)
    on_off = models.BooleanField(null=True, blank=True)

    def __str__(self):
        return '%s, %s, %s, %s, %s, %s, %s, %s, %s' % (self.name, self.photo, self.contents, self.description, self.price)


class Sections(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    place_id = models.ForeignKey('Place', on_delete=models.CASCADE)

    def __str__(self):
        return '%s' % (self.name)

class Ingredients(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)

    def __str__(self):
        return '%s' % (self.name)


class Contents(models.Model):
    id = models.AutoField(primary_key=True)
    ingredient_id = models.ForeignKey('Ingredients', on_delete=models.CASCADE)
    menu_id = models.ForeignKey('Menu', on_delete=models.CASCADE)
    amount = models.IntegerField(null=True)

    def __str__(self):
        return '%s' % (self.amount)


class Tables(models.Model):
    id = models.AutoField(primary_key=True)
    number = models.IntegerField(null=True)
    place_id = models.ForeignKey('Place', on_delete=models.CASCADE)

    def __str__(self):
        return '%s' % (self.number)

class Waiters(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    surname = models.CharField(max_length=100)
    gender =  models.BooleanField()
    on_shift = models.BooleanField()
    is_admin = models.BooleanField()
    password = models.CharField(max_length=128)
    place_id =  models.ForeignKey('Place', on_delete=models.CASCADE)

    def __str__(self):
        return '%s, %s, %s, %s, %s, %s' % (self.name, self.surname, self.gender, self.on_shift, self.is_admin, self.password)


class Orders(models.Model):
    id = models.AutoField(primary_key=True)
    table_id = models.ForeignKey('Tables', on_delete=models.CASCADE)
    waiter_id = models.ForeignKey('Waiters', on_delete=models.CASCADE)
    time_in = models.TimeField(auto_now=False, auto_now_add=False)
    accept_time = models.TimeField(auto_now=False, auto_now_add=False)
    waiting_time = models.TimeField(auto_now=False, auto_now_add=False)

    def __str__(self):
        return '%s, %s, %s' % (self.time_in, self.accept_time, self.waiting_time)

    
class Country(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=100)

    def __str__(self):
        return '%s, %s' % (self.name, self.code)
 

class City(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    country_id = models.ForeignKey('Country', on_delete=models.CASCADE)

    def __str__(self):
        return '%s' % (self.name)


class Orders_item(models.Model):
    id = models.AutoField(primary_key=True)
    order_id = models.ForeignKey('Orders', on_delete=models.CASCADE)
    menu_id = models.ForeignKey('Menu', on_delete=models.CASCADE)

   




