from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from . import models
from . import backend_serializers
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated


class ViewPlace(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user
        result = None
        result_status = status.HTTP_404_NOT_FOUND

        if view_id is None:
            result = []
        else:
            return_status = status.HTTP_403_FORBIDDEN

        if user.is_authenticated:
            if view_id is None:
                owned = [dash for dash in models.Place.objects.filter(owner_id = user)]
                editable = [dash for dash in models.Place.objects.filter(can_edit = user)]
                viewable = [dash for dash in models.Place.objects.filter(can_view = user)]
                uniq = list(set(owned + editable + viewable))

                serializer = backend_serializers.PlaceSerializer()
                result =[backend_serializers.PlaceSerializer(dash).data for dash in uniq]
                if len(result) > 0:
                    result = list(map(lambda x: dict(**x, editable=((x['owner_id'] == user.pk) or (user.pk in x['can_view']))), result))
                    return_status = status.HTTP_200_OK
            else:
                dash = models.Place.objects.filter(pk = view_id)
                if not dash.exists():
                    return_status = status.HTTP_404_NOT_FOUND
                elif (dash.filter(can_edit = user).exists() or
                    dash.filter(can_view = user).exists() or
                    dash.filter(owner_id = user).exists()):
                    result = backend_serializers.PlaceSerializer(dash[0]).data
                    result['editable'] = (dash.filter(owner_id = user).exists() or dash.filter(can_view = user). exists())
                    return_status = ststus.HTTP_200_OK
                else:
                    return_status = status.HTTP_403_FORBIDDEN

        else:
            return_status = status.HTTP_401_UNAUTHORIZED

        return JsonResponse(result, status=return_status, safe=False)


    def delete(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user

        result = None
        result_status = status.HTTP_404_NOT_FOUND

        if view_id is None:
            return JsonResponse(result, status=return_status, safe=False)

        if user.is_authenticated:
            dash = models.Place.objects.filter(pk = view_id)
            if not dash.exists():
                return_status = status.HTTP_404_NOT_FOUND
            elif (dash.filter(can_edit = user).exists() or
                dash.filter(owner_id = user).exists()):
                result = backend_serializers.PlaceSerializer(dash[0]).data
                dash[0].delete()
                return_status = status.HTTP_200_OK
            else:
                return_status = status.HTTP_403_FORBIDDEN
        else:
            return_status = status.HTTP_403_FORBIDDEN

        return JsonResponse(result, status=return_status, safe=False)

   
    def put(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user
        result = None
        serializer = None
        return_status = status.HTTP_200_OK

        if user.is_authenticated:
            if view_id is not None:
                dash = models.Place.objects.filter(pk = view_id)
                if not dash.exists():
                    return_status = status.HTTP_404_NOT_FOUND
                elif (dash.filter(can_edit = user).exists() or
                    dash.filter(owner_id = user).exists()):
                    dash = models.Place.objects.filter(pk = view_id)
                    data = request.data
                    data['owner_id'] = dash[0].owner_id.pk;
                    serializer = backend_serializers.PlaceSerializer(dash[0], data=data)
                else:
                    print("Place doesn't belong to the user")
                    print_status = status.HTTP_403_FORBIDDEN
            else:
                dash = models.Place.objects.create(owner_id = user)
                data = request.data
                data['owner_id'] = user.pk
                serializer = backend_serializers.PlaceSerializer(dash, data=data)

            if serializer is not None:
                if serializer.is_valid():
                    serializer.save()
                    result = serializer.data
                    result['editable'] = True
                else:
                    result = {"errors": serializer.errors}
                    return_status = status.HTTP_400_BAD_REQUEST
        else:
            print("User is not authenticated");
            return_status = status.HTTP_403_FORBIDDEN

        return JsonResponse(result, status=return_status, safe=False)





class ViewMenu(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user
        result = None
        result_status = status.HTTP_404_NOT_FOUND

        if view_id is None:
            result = []
        else:
            return_status = status.HTTP_403_FORBIDDEN

        if user.is_authenticated:
            if view_id is None:
                owned = [dash for dash in models.Menu.objects.filter(owner_id = user)]
                editable = [dash for dash in models.Menu.objects.filter(can_edit = user)]
                viewable = [dash for dash in models.Menu.objects.filter(can_view = user)]
                uniq = list(set(owned + editable + viewable))

                serializer = backend_serializers.MenuSerializer()
                result =[backend_serializers.MenuSerializer(dash).data for dash in uniq]
                if len(result) > 0:
                    result = list(map(lambda x: dict(**x, editable=((x['owner_id'] == user.pk) or (user.pk in x['can_view']))), result))
                    return_status = status.HTTP_200_OK
            else:
                dash = models.Menu.objects.filter(pk = view_id)
                if not dash.exists():
                    return_status = status.HTTP_404_NOT_FOUND
                elif (dash.filter(can_edit = user).exists() or
                    dash.filter(can_view = user).exists() or
                    dash.filter(owner_id = user).exists()):
                    result = backend_serializers.MenuSerializer(dash[0]).data
                    result['editable'] = (dash.filter(owner_id = user).exists() or dash.filter(can_view = user). exists())
                    return_status = ststus.HTTP_200_OK
                else:
                    return_status = status.HTTP_403_FORBIDDEN

        else:
            return_status = status.HTTP_401_UNAUTHORIZED

        return JsonResponse(result, status=return_status, safe=False)


    def delete(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user

        result = None
        result_status = status.HTTP_404_NOT_FOUND

        if view_id is None:
            return JsonResponse(result, status=return_status, safe=False)

        if user.is_authenticated:
            dash = models.Menu.objects.filter(pk = view_id)
            if not dash.exists():
                return_status = status.HTTP_404_NOT_FOUND
            elif (dash.filter(can_edit = user).exists() or
                dash.filter(owner_id = user).exists()):
                result = backend_serializers.MenuSerializer(dash[0]).data
                dash[0].delete()
                return_status = status.HTTP_200_OK
            else:
                return_status = status.HTTP_403_FORBIDDEN
        else:
            return_status = status.HTTP_403_FORBIDDEN

        return JsonResponse(result, status=return_status, safe=False)

   
    def put(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user
        result = None
        serializer = None
        return_status = status.HTTP_200_OK

        if user.is_authenticated:
            if view_id is not None:
                dash = models.Menu.objects.filter(pk = view_id)
                if not dash.exists():
                    return_status = status.HTTP_404_NOT_FOUND
                elif (dash.filter(can_edit = user).exists() or
                    dash.filter(owner_id = user).exists()):
                    dash = models.Menu.objects.filter(pk = view_id)
                    data = request.data
                    data['owner_id'] = dash[0].owner_id.pk;
                    serializer = backend_serializers.MenuSerializer(dash[0], data=data)
                else:
                    print("Menu doesn't belong to the user")
                    print_status = status.HTTP_403_FORBIDDEN
            else:
                dash = models.Menu.objects.create(owner_id = user)
                data = request.data
                data['owner_id'] = user.pk
                serializer = backend_serializers.MenuSerializer(dash, data=data)

            if serializer is not None:
                if serializer.is_valid():
                    serializer.save()
                    result = serializer.data
                    result['editable'] = True
                else:
                    result = {"errors": serializer.errors}
                    return_status = status.HTTP_400_BAD_REQUEST
        else:
            print("User is not authenticated");
            return_status = status.HTTP_403_FORBIDDEN

        return JsonResponse(result, status=return_status, safe=False)


class ViewSections(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user
        result = None
        result_status = status.HTTP_404_NOT_FOUND

        if view_id is None:
            result = []
        else:
            return_status = status.HTTP_403_FORBIDDEN

        if user.is_authenticated:
            if view_id is None:
                owned = [dash for dash in models.Sections.objects.filter(owner_id = user)]
                editable = [dash for dash in models.Sections.objects.filter(can_edit = user)]
                viewable = [dash for dash in models.Sections.objects.filter(can_view = user)]
                uniq = list(set(owned + editable + viewable))

                serializer = backend_serializers.SectionsSerializer()
                result =[backend_serializers.SectionsSerializer(dash).data for dash in uniq]
                if len(result) > 0:
                    result = list(map(lambda x: dict(**x, editable=((x['owner_id'] == user.pk) or (user.pk in x['can_view']))), result))
                    return_status = status.HTTP_200_OK
            else:
                dash = models.Sections.objects.filter(pk = view_id)
                if not dash.exists():
                    return_status = status.HTTP_404_NOT_FOUND
                elif (dash.filter(can_edit = user).exists() or
                    dash.filter(can_view = user).exists() or
                    dash.filter(owner_id = user).exists()):
                    result = backend_serializers.PlaceSerializer(dash[0]).data
                    result['editable'] = (dash.filter(owner_id = user).exists() or dash.filter(can_view = user). exists())
                    return_status = ststus.HTTP_200_OK
                else:
                    return_status = status.HTTP_403_FORBIDDEN

        else:
            return_status = status.HTTP_401_UNAUTHORIZED

        return JsonResponse(result, status=return_status, safe=False)


    def delete(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user

        result = None
        result_status = status.HTTP_404_NOT_FOUND

        if view_id is None:
            return JsonResponse(result, status=return_status, safe=False)

        if user.is_authenticated:
            dash = models.Sections.objects.filter(pk = view_id)
            if not dash.exists():
                return_status = status.HTTP_404_NOT_FOUND
            elif (dash.filter(can_edit = user).exists() or
                dash.filter(owner_id = user).exists()):
                result = backend_serializers.SectionsSerializer(dash[0]).data
                dash[0].delete()
                return_status = status.HTTP_200_OK
            else:
                return_status = status.HTTP_403_FORBIDDEN
        else:
            return_status = status.HTTP_403_FORBIDDEN

        return JsonResponse(result, status=return_status, safe=False)

   
    def put(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user
        result = None
        serializer = None
        return_status = status.HTTP_200_OK

        if user.is_authenticated:
            if view_id is not None:
                dash = models.Sections.objects.filter(pk = view_id)
                if not dash.exists():
                    return_status = status.HTTP_404_NOT_FOUND
                elif (dash.filter(can_edit = user).exists() or
                    dash.filter(owner_id = user).exists()):
                    dash = models.Sections.objects.filter(pk = view_id)
                    data = request.data
                    data['owner_id'] = dash[0].owner_id.pk;
                    serializer = backend_serializers.SectionsSerializer(dash[0], data=data)
                else:
                    print("Sections doesn't belong to the user")
                    print_status = status.HTTP_403_FORBIDDEN
            else:
                dash = models.Sections.objects.create(owner_id = user)
                data = request.data
                data['owner_id'] = user.pk
                serializer = backend_serializers.SectionsSerializer(dash, data=data)

            if serializer is not None:
                if serializer.is_valid():
                    serializer.save()
                    result = serializer.data
                    result['editable'] = True
                else:
                    result = {"errors": serializer.errors}
                    return_status = status.HTTP_400_BAD_REQUEST
        else:
            print("User is not authenticated");
            return_status = status.HTTP_403_FORBIDDEN

        return JsonResponse(result, status=return_status, safe=False)




class ViewIngredients(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user
        result = None
        result_status = status.HTTP_404_NOT_FOUND

        if view_id is None:
            result = []
        else:
            return_status = status.HTTP_403_FORBIDDEN

        if user.is_authenticated:
            if view_id is None:
                owned = [dash for dash in models.Ingredients.objects.filter(owner_id = user)]
                editable = [dash for dash in models.Ingredients.objects.filter(can_edit = user)]
                viewable = [dash for dash in models.Ingredients.objects.filter(can_view = user)]
                uniq = list(set(owned + editable + viewable))

                serializer = backend_serializers.IngredientsSerializer()
                result =[backend_serializers.IngredientsSerializer(dash).data for dash in uniq]
                if len(result) > 0:
                    result = list(map(lambda x: dict(**x, editable=((x['owner_id'] == user.pk) or (user.pk in x['can_view']))), result))
                    return_status = status.HTTP_200_OK
            else:
                dash = models.Ingredients.objects.filter(pk = view_id)
                if not dash.exists():
                    return_status = status.HTTP_404_NOT_FOUND
                elif (dash.filter(can_edit = user).exists() or
                    dash.filter(can_view = user).exists() or
                    dash.filter(owner_id = user).exists()):
                    result = backend_serializers.IngredientsSerializer(dash[0]).data
                    result['editable'] = (dash.filter(owner_id = user).exists() or dash.filter(can_view = user). exists())
                    return_status = ststus.HTTP_200_OK
                else:
                    return_status = status.HTTP_403_FORBIDDEN

        else:
            return_status = status.HTTP_401_UNAUTHORIZED

        return JsonResponse(result, status=return_status, safe=False)


    def delete(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user

        result = None
        result_status = status.HTTP_404_NOT_FOUND

        if view_id is None:
            return JsonResponse(result, status=return_status, safe=False)

        if user.is_authenticated:
            dash = models.Ingredients.objects.filter(pk = view_id)
            if not dash.exists():
                return_status = status.HTTP_404_NOT_FOUND
            elif (dash.filter(can_edit = user).exists() or
                dash.filter(owner_id = user).exists()):
                result = backend_serializers.IngredientsSerializer(dash[0]).data
                dash[0].delete()
                return_status = status.HTTP_200_OK
            else:
                return_status = status.HTTP_403_FORBIDDEN
        else:
            return_status = status.HTTP_403_FORBIDDEN

        return JsonResponse(result, status=return_status, safe=False)

   
    def put(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user
        result = None
        serializer = None
        return_status = status.HTTP_200_OK

        if user.is_authenticated:
            if view_id is not None:
                dash = models.Ingredients.objects.filter(pk = view_id)
                if not dash.exists():
                    return_status = status.HTTP_404_NOT_FOUND
                elif (dash.filter(can_edit = user).exists() or
                    dash.filter(owner_id = user).exists()):
                    dash = models.Ingredients.objects.filter(pk = view_id)
                    data = request.data
                    data['owner_id'] = dash[0].owner_id.pk;
                    serializer = backend_serializers.IngredientsSerializer(dash[0], data=data)
                else:
                    print("Sections doesn't belong to the user")
                    print_status = status.HTTP_403_FORBIDDEN
            else:
                dash = models.Ingredients.objects.create(owner_id = user)
                data = request.data
                data['owner_id'] = user.pk
                serializer = backend_serializers.IngredientsSerializer(dash, data=data)

            if serializer is not None:
                if serializer.is_valid():
                    serializer.save()
                    result = serializer.data
                    result['editable'] = True
                else:
                    result = {"errors": serializer.errors}
                    return_status = status.HTTP_400_BAD_REQUEST
        else:
            print("User is not authenticated");
            return_status = status.HTTP_403_FORBIDDEN

        return JsonResponse(result, status=return_status, safe=False)




class ViewContents(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user
        result = None
        result_status = status.HTTP_404_NOT_FOUND

        if view_id is None:
            result = []
        else:
            return_status = status.HTTP_403_FORBIDDEN

        if user.is_authenticated:
            if view_id is None:
                owned = [dash for dash in models.Contents.objects.filter(owner_id = user)]
                editable = [dash for dash in models.Contents.objects.filter(can_edit = user)]
                viewable = [dash for dash in models.Contents.objects.filter(can_view = user)]
                uniq = list(set(owned + editable + viewable))

                serializer = backend_serializers.ContentsSerializer()
                result =[backend_serializers.ContentsSerializer(dash).data for dash in uniq]
                if len(result) > 0:
                    result = list(map(lambda x: dict(**x, editable=((x['owner_id'] == user.pk) or (user.pk in x['can_view']))), result))
                    return_status = status.HTTP_200_OK
            else:
                dash = models.Contents.objects.filter(pk = view_id)
                if not dash.exists():
                    return_status = status.HTTP_404_NOT_FOUND
                elif (dash.filter(can_edit = user).exists() or
                    dash.filter(can_view = user).exists() or
                    dash.filter(owner_id = user).exists()):
                    result = backend_serializers.ContentsSerializer(dash[0]).data
                    result['editable'] = (dash.filter(owner_id = user).exists() or dash.filter(can_view = user). exists())
                    return_status = ststus.HTTP_200_OK
                else:
                    return_status = status.HTTP_403_FORBIDDEN

        else:
            return_status = status.HTTP_401_UNAUTHORIZED

        return JsonResponse(result, status=return_status, safe=False)


    def delete(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user

        result = None
        result_status = status.HTTP_404_NOT_FOUND

        if view_id is None:
            return JsonResponse(result, status=return_status, safe=False)

        if user.is_authenticated:
            dash = models.Ingredients.objects.filter(pk = view_id)
            if not dash.exists():
                return_status = status.HTTP_404_NOT_FOUND
            elif (dash.filter(can_edit = user).exists() or
                dash.filter(owner_id = user).exists()):
                result = backend_serializers.ContentsSerializer(dash[0]).data
                dash[0].delete()
                return_status = status.HTTP_200_OK
            else:
                return_status = status.HTTP_403_FORBIDDEN
        else:
            return_status = status.HTTP_403_FORBIDDEN

        return JsonResponse(result, status=return_status, safe=False)

   
    def put(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user
        result = None
        serializer = None
        return_status = status.HTTP_200_OK

        if user.is_authenticated:
            if view_id is not None:
                dash = models.Contents.objects.filter(pk = view_id)
                if not dash.exists():
                    return_status = status.HTTP_404_NOT_FOUND
                elif (dash.filter(can_edit = user).exists() or
                    dash.filter(owner_id = user).exists()):
                    dash = models.Contents.objects.filter(pk = view_id)
                    data = request.data
                    data['owner_id'] = dash[0].owner_id.pk;
                    serializer = backend_serializers.ContentsSerializer(dash[0], data=data)
                else:
                    print("Contents doesn't belong to the user")
                    print_status = status.HTTP_403_FORBIDDEN
            else:
                dash = models.Contents.objects.create(owner_id = user)
                data = request.data
                data['owner_id'] = user.pk
                serializer = backend_serializers.ContentsSerializer(dash, data=data)

            if serializer is not None:
                if serializer.is_valid():
                    serializer.save()
                    result = serializer.data
                    result['editable'] = True
                else:
                    result = {"errors": serializer.errors}
                    return_status = status.HTTP_400_BAD_REQUEST
        else:
            print("User is not authenticated");
            return_status = status.HTTP_403_FORBIDDEN

        return JsonResponse(result, status=return_status, safe=False)



class ViewTables(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user
        result = None
        result_status = status.HTTP_404_NOT_FOUND

        if view_id is None:
            result = []
        else:
            return_status = status.HTTP_403_FORBIDDEN

        if user.is_authenticated:
            if view_id is None:
                owned = [dash for dash in models.Tables.objects.filter(owner_id = user)]
                editable = [dash for dash in models.Tables.objects.filter(can_edit = user)]
                viewable = [dash for dash in models.Tables.objects.filter(can_view = user)]
                uniq = list(set(owned + editable + viewable))

                serializer = backend_serializers.TablesSerializer()
                result =[backend_serializers.TablesSerializer(dash).data for dash in uniq]
                if len(result) > 0:
                    result = list(map(lambda x: dict(**x, editable=((x['owner_id'] == user.pk) or (user.pk in x['can_view']))), result))
                    return_status = status.HTTP_200_OK
            else:
                dash = models.Tables.objects.filter(pk = view_id)
                if not dash.exists():
                    return_status = status.HTTP_404_NOT_FOUND
                elif (dash.filter(can_edit = user).exists() or
                    dash.filter(can_view = user).exists() or
                    dash.filter(owner_id = user).exists()):
                    result = backend_serializers.TablesSerializer(dash[0]).data
                    result['editable'] = (dash.filter(owner_id = user).exists() or dash.filter(can_view = user). exists())
                    return_status = ststus.HTTP_200_OK
                else:
                    return_status = status.HTTP_403_FORBIDDEN

        else:
            return_status = status.HTTP_401_UNAUTHORIZED

        return JsonResponse(result, status=return_status, safe=False)


    def delete(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user

        result = None
        result_status = status.HTTP_404_NOT_FOUND

        if view_id is None:
            return JsonResponse(result, status=return_status, safe=False)

        if user.is_authenticated:
            dash = models.Tables.objects.filter(pk = view_id)
            if not dash.exists():
                return_status = status.HTTP_404_NOT_FOUND
            elif (dash.filter(can_edit = user).exists() or
                dash.filter(owner_id = user).exists()):
                result = backend_serializers.TablesSerializer(dash[0]).data
                dash[0].delete()
                return_status = status.HTTP_200_OK
            else:
                return_status = status.HTTP_403_FORBIDDEN
        else:
            return_status = status.HTTP_403_FORBIDDEN

        return JsonResponse(result, status=return_status, safe=False)

   
    def put(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user
        result = None
        serializer = None
        return_status = status.HTTP_200_OK

        if user.is_authenticated:
            if view_id is not None:
                dash = models.Tables.objects.filter(pk = view_id)
                if not dash.exists():
                    return_status = status.HTTP_404_NOT_FOUND
                elif (dash.filter(can_edit = user).exists() or
                    dash.filter(owner_id = user).exists()):
                    dash = models.Tables.objects.filter(pk = view_id)
                    data = request.data
                    data['owner_id'] = dash[0].owner_id.pk;
                    serializer = backend_serializers.TablesSerializer(dash[0], data=data)
                else:
                    print("Tables doesn't belong to the user")
                    print_status = status.HTTP_403_FORBIDDEN
            else:
                dash = models.Tables.objects.create(owner_id = user)
                data = request.data
                data['owner_id'] = user.pk
                serializer = backend_serializers.TablesSerializer(dash, data=data)

            if serializer is not None:
                if serializer.is_valid():
                    serializer.save()
                    result = serializer.data
                    result['editable'] = True
                else:
                    result = {"errors": serializer.errors}
                    return_status = status.HTTP_400_BAD_REQUEST
        else:
            print("User is not authenticated");
            return_status = status.HTTP_403_FORBIDDEN

        return JsonResponse(result, status=return_status, safe=False)




class ViewWaiters(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user
        result = None
        result_status = status.HTTP_404_NOT_FOUND

        if view_id is None:
            result = []
        else:
            return_status = status.HTTP_403_FORBIDDEN

        if user.is_authenticated:
            if view_id is None:
                owned = [dash for dash in models.Waiters.objects.filter(owner_id = user)]
                editable = [dash for dash in models.Waiters.objects.filter(can_edit = user)]
                viewable = [dash for dash in models.Waiters.objects.filter(can_view = user)]
                uniq = list(set(owned + editable + viewable))

                serializer = backend_serializers.WaitersSerializer()
                result =[backend_serializers.WaitersSerializer(dash).data for dash in uniq]
                if len(result) > 0:
                    result = list(map(lambda x: dict(**x, editable=((x['owner_id'] == user.pk) or (user.pk in x['can_view']))), result))
                    return_status = status.HTTP_200_OK
            else:
                dash = models.Waiters.objects.filter(pk = view_id)
                if not dash.exists():
                    return_status = status.HTTP_404_NOT_FOUND
                elif (dash.filter(can_edit = user).exists() or
                    dash.filter(can_view = user).exists() or
                    dash.filter(owner_id = user).exists()):
                    result = backend_serializers.WaitersSerializer(dash[0]).data
                    result['editable'] = (dash.filter(owner_id = user).exists() or dash.filter(can_view = user). exists())
                    return_status = ststus.HTTP_200_OK
                else:
                    return_status = status.HTTP_403_FORBIDDEN

        else:
            return_status = status.HTTP_401_UNAUTHORIZED

        return JsonResponse(result, status=return_status, safe=False)


    def delete(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user

        result = None
        result_status = status.HTTP_404_NOT_FOUND

        if view_id is None:
            return JsonResponse(result, status=return_status, safe=False)

        if user.is_authenticated:
            dash = models.Waiters.objects.filter(pk = view_id)
            if not dash.exists():
                return_status = status.HTTP_404_NOT_FOUND
            elif (dash.filter(can_edit = user).exists() or
                dash.filter(owner_id = user).exists()):
                result = backend_serializers.WaitersSerializer(dash[0]).data
                dash[0].delete()
                return_status = status.HTTP_200_OK
            else:
                return_status = status.HTTP_403_FORBIDDEN
        else:
            return_status = status.HTTP_403_FORBIDDEN

        return JsonResponse(result, status=return_status, safe=False)

   
    def put(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user
        result = None
        serializer = None
        return_status = status.HTTP_200_OK

        if user.is_authenticated:
            if view_id is not None:
                dash = models.Waiters.objects.filter(pk = view_id)
                if not dash.exists():
                    return_status = status.HTTP_404_NOT_FOUND
                elif (dash.filter(can_edit = user).exists() or
                    dash.filter(owner_id = user).exists()):
                    dash = models.Waiters.objects.filter(pk = view_id)
                    data = request.data
                    data['owner_id'] = dash[0].owner_id.pk;
                    serializer = backend_serializers.WaitersSerializer(dash[0], data=data)
                else:
                    print("Waiters doesn't belong to the user")
                    print_status = status.HTTP_403_FORBIDDEN
            else:
                dash = models.Waiters.objects.create(owner_id = user)
                data = request.data
                data['owner_id'] = user.pk
                serializer = backend_serializers.WaitersSerializer(dash, data=data)

            if serializer is not None:
                if serializer.is_valid():
                    serializer.save()
                    result = serializer.data
                    result['editable'] = True
                else:
                    result = {"errors": serializer.errors}
                    return_status = status.HTTP_400_BAD_REQUEST
        else:
            print("User is not authenticated");
            return_status = status.HTTP_403_FORBIDDEN

        return JsonResponse(result, status=return_status, safe=False)




class ViewOrders(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user
        result = None
        result_status = status.HTTP_404_NOT_FOUND

        if view_id is None:
            result = []
        else:
            return_status = status.HTTP_403_FORBIDDEN

        if user.is_authenticated:
            if view_id is None:
                owned = [dash for dash in models.Orders.objects.filter(owner_id = user)]
                editable = [dash for dash in models.Orders.objects.filter(can_edit = user)]
                viewable = [dash for dash in models.Orders.objects.filter(can_view = user)]
                uniq = list(set(owned + editable + viewable))

                serializer = backend_serializers.OrdersSerializer()
                result =[backend_serializers.OrdersSerializer(dash).data for dash in uniq]
                if len(result) > 0:
                    result = list(map(lambda x: dict(**x, editable=((x['owner_id'] == user.pk) or (user.pk in x['can_view']))), result))
                    return_status = status.HTTP_200_OK
            else:
                dash = models.Orders.objects.filter(pk = view_id)
                if not dash.exists():
                    return_status = status.HTTP_404_NOT_FOUND
                elif (dash.filter(can_edit = user).exists() or
                    dash.filter(can_view = user).exists() or
                    dash.filter(owner_id = user).exists()):
                    result = backend_serializers.OrdersSerializer(dash[0]).data
                    result['editable'] = (dash.filter(owner_id = user).exists() or dash.filter(can_view = user). exists())
                    return_status = ststus.HTTP_200_OK
                else:
                    return_status = status.HTTP_403_FORBIDDEN

        else:
            return_status = status.HTTP_401_UNAUTHORIZED

        return JsonResponse(result, status=return_status, safe=False)


    def delete(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user

        result = None
        result_status = status.HTTP_404_NOT_FOUND

        if view_id is None:
            return JsonResponse(result, status=return_status, safe=False)

        if user.is_authenticated:
            dash = models.Orders.objects.filter(pk = view_id)
            if not dash.exists():
                return_status = status.HTTP_404_NOT_FOUND
            elif (dash.filter(can_edit = user).exists() or
                dash.filter(owner_id = user).exists()):
                result = backend_serializers.OrdersSerializer(dash[0]).data
                dash[0].delete()
                return_status = status.HTTP_200_OK
            else:
                return_status = status.HTTP_403_FORBIDDEN
        else:
            return_status = status.HTTP_403_FORBIDDEN

        return JsonResponse(result, status=return_status, safe=False)

   
    def put(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user
        result = None
        serializer = None
        return_status = status.HTTP_200_OK

        if user.is_authenticated:
            if view_id is not None:
                dash = models.Orders.objects.filter(pk = view_id)
                if not dash.exists():
                    return_status = status.HTTP_404_NOT_FOUND
                elif (dash.filter(can_edit = user).exists() or
                    dash.filter(owner_id = user).exists()):
                    dash = models.Orders.objects.filter(pk = view_id)
                    data = request.data
                    data['owner_id'] = dash[0].owner_id.pk;
                    serializer = backend_serializers.OrdersSerializer(dash[0], data=data)
                else:
                    print("Orders doesn't belong to the user")
                    print_status = status.HTTP_403_FORBIDDEN
            else:
                dash = models.Orders.objects.create(owner_id = user)
                data = request.data
                data['owner_id'] = user.pk
                serializer = backend_serializers.OrdersSerializer(dash, data=data)

            if serializer is not None:
                if serializer.is_valid():
                    serializer.save()
                    result = serializer.data
                    result['editable'] = True
                else:
                    result = {"errors": serializer.errors}
                    return_status = status.HTTP_400_BAD_REQUEST
        else:
            print("User is not authenticated");
            return_status = status.HTTP_403_FORBIDDEN

        return JsonResponse(result, status=return_status, safe=False)



class ViewCountry(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user
        result = None
        result_status = status.HTTP_404_NOT_FOUND

        if view_id is None:
            result = []
        else:
            return_status = status.HTTP_403_FORBIDDEN

        if user.is_authenticated:
            if view_id is None:
                owned = [dash for dash in models.Country.objects.filter(owner_id = user)]
                editable = [dash for dash in models.Country.objects.filter(can_edit = user)]
                viewable = [dash for dash in models.Country.objects.filter(can_view = user)]
                uniq = list(set(owned + editable + viewable))

                serializer = backend_serializers.CountrySerializer()
                result =[backend_serializers.CountrySerializer(dash).data for dash in uniq]
                if len(result) > 0:
                    result = list(map(lambda x: dict(**x, editable=((x['owner_id'] == user.pk) or (user.pk in x['can_view']))), result))
                    return_status = status.HTTP_200_OK
            else:
                dash = models.Country.objects.filter(pk = view_id)
                if not dash.exists():
                    return_status = status.HTTP_404_NOT_FOUND
                elif (dash.filter(can_edit = user).exists() or
                    dash.filter(can_view = user).exists() or
                    dash.filter(owner_id = user).exists()):
                    result = backend_serializers.CountrySerializer(dash[0]).data
                    result['editable'] = (dash.filter(owner_id = user).exists() or dash.filter(can_view = user). exists())
                    return_status = ststus.HTTP_200_OK
                else:
                    return_status = status.HTTP_403_FORBIDDEN

        else:
            return_status = status.HTTP_401_UNAUTHORIZED

        return JsonResponse(result, status=return_status, safe=False)


    def delete(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user

        result = None
        result_status = status.HTTP_404_NOT_FOUND

        if view_id is None:
            return JsonResponse(result, status=return_status, safe=False)

        if user.is_authenticated:
            dash = models.Country.objects.filter(pk = view_id)
            if not dash.exists():
                return_status = status.HTTP_404_NOT_FOUND
            elif (dash.filter(can_edit = user).exists() or
                dash.filter(owner_id = user).exists()):
                result = backend_serializers.CountrySerializer(dash[0]).data
                dash[0].delete()
                return_status = status.HTTP_200_OK
            else:
                return_status = status.HTTP_403_FORBIDDEN
        else:
            return_status = status.HTTP_403_FORBIDDEN

        return JsonResponse(result, status=return_status, safe=False)

   
    def put(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user
        result = None
        serializer = None
        return_status = status.HTTP_200_OK

        if user.is_authenticated:
            if view_id is not None:
                dash = models.Country.objects.filter(pk = view_id)
                if not dash.exists():
                    return_status = status.HTTP_404_NOT_FOUND
                elif (dash.filter(can_edit = user).exists() or
                    dash.filter(owner_id = user).exists()):
                    dash = models.Country.objects.filter(pk = view_id)
                    data = request.data
                    data['owner_id'] = dash[0].owner_id.pk;
                    serializer = backend_serializers.CountrySerializer(dash[0], data=data)
                else:
                    print("Country doesn't belong to the user")
                    print_status = status.HTTP_403_FORBIDDEN
            else:
                dash = models.Country.objects.create(owner_id = user)
                data = request.data
                data['owner_id'] = user.pk
                serializer = backend_serializers.CountrySerializer(dash, data=data)

            if serializer is not None:
                if serializer.is_valid():
                    serializer.save()
                    result = serializer.data
                    result['editable'] = True
                else:
                    result = {"errors": serializer.errors}
                    return_status = status.HTTP_400_BAD_REQUEST
        else:
            print("User is not authenticated");
            return_status = status.HTTP_403_FORBIDDEN

        return JsonResponse(result, status=return_status, safe=False)




class ViewCity(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user
        result = None
        result_status = status.HTTP_404_NOT_FOUND

        if view_id is None:
            result = []
        else:
            return_status = status.HTTP_403_FORBIDDEN

        if user.is_authenticated:
            if view_id is None:
                owned = [dash for dash in models.City.objects.filter(owner_id = user)]
                editable = [dash for dash in models.City.objects.filter(can_edit = user)]
                viewable = [dash for dash in models.City.objects.filter(can_view = user)]
                uniq = list(set(owned + editable + viewable))

                serializer = backend_serializers.CitySerializer()
                result =[backend_serializers.CitySerializer(dash).data for dash in uniq]
                if len(result) > 0:
                    result = list(map(lambda x: dict(**x, editable=((x['owner_id'] == user.pk) or (user.pk in x['can_view']))), result))
                    return_status = status.HTTP_200_OK
            else:
                dash = models.City.objects.filter(pk = view_id)
                if not dash.exists():
                    return_status = status.HTTP_404_NOT_FOUND
                elif (dash.filter(can_edit = user).exists() or
                    dash.filter(can_view = user).exists() or
                    dash.filter(owner_id = user).exists()):
                    result = backend_serializers.CitySerializer(dash[0]).data
                    result['editable'] = (dash.filter(owner_id = user).exists() or dash.filter(can_view = user). exists())
                    return_status = ststus.HTTP_200_OK
                else:
                    return_status = status.HTTP_403_FORBIDDEN

        else:
            return_status = status.HTTP_401_UNAUTHORIZED

        return JsonResponse(result, status=return_status, safe=False)


    def delete(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user

        result = None
        result_status = status.HTTP_404_NOT_FOUND

        if view_id is None:
            return JsonResponse(result, status=return_status, safe=False)

        if user.is_authenticated:
            dash = models.City.objects.filter(pk = view_id)
            if not dash.exists():
                return_status = status.HTTP_404_NOT_FOUND
            elif (dash.filter(can_edit = user).exists() or
                dash.filter(owner_id = user).exists()):
                result = backend_serializers.CitySerializer(dash[0]).data
                dash[0].delete()
                return_status = status.HTTP_200_OK
            else:
                return_status = status.HTTP_403_FORBIDDEN
        else:
            return_status = status.HTTP_403_FORBIDDEN

        return JsonResponse(result, status=return_status, safe=False)

   
    def put(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user
        result = None
        serializer = None
        return_status = status.HTTP_200_OK

        if user.is_authenticated:
            if view_id is not None:
                dash = models.City.objects.filter(pk = view_id)
                if not dash.exists():
                    return_status = status.HTTP_404_NOT_FOUND
                elif (dash.filter(can_edit = user).exists() or
                    dash.filter(owner_id = user).exists()):
                    dash = models.City.objects.filter(pk = view_id)
                    data = request.data
                    data['owner_id'] = dash[0].owner_id.pk;
                    serializer = backend_serializers.CitySerializer(dash[0], data=data)
                else:
                    print("City doesn't belong to the user")
                    print_status = status.HTTP_403_FORBIDDEN
            else:
                dash = models.City.objects.create(owner_id = user)
                data = request.data
                data['owner_id'] = user.pk
                serializer = backend_serializers.CitySerializer(dash, data=data)

            if serializer is not None:
                if serializer.is_valid():
                    serializer.save()
                    result = serializer.data
                    result['editable'] = True
                else:
                    result = {"errors": serializer.errors}
                    return_status = status.HTTP_400_BAD_REQUEST
        else:
            print("User is not authenticated");
            return_status = status.HTTP_403_FORBIDDEN

        return JsonResponse(result, status=return_status, safe=False)



class ViewOrders_item(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user
        result = None
        result_status = status.HTTP_404_NOT_FOUND

        if view_id is None:
            result = []
        else:
            return_status = status.HTTP_403_FORBIDDEN

        if user.is_authenticated:
            if view_id is None:
                owned = [dash for dash in models.Orders_item.objects.filter(owner_id = user)]
                editable = [dash for dash in models.Orders_item.objects.filter(can_edit = user)]
                viewable = [dash for dash in models.Orders_item.objects.filter(can_view = user)]
                uniq = list(set(owned + editable + viewable))

                serializer = backend_serializers.Orders_itemSerializer()
                result =[backend_serializers.Orders_itemSerializer(dash).data for dash in uniq]
                if len(result) > 0:
                    result = list(map(lambda x: dict(**x, editable=((x['owner_id'] == user.pk) or (user.pk in x['can_view']))), result))
                    return_status = status.HTTP_200_OK
            else:
                dash = models.Orders_item.objects.filter(pk = view_id)
                if not dash.exists():
                    return_status = status.HTTP_404_NOT_FOUND
                elif (dash.filter(can_edit = user).exists() or
                    dash.filter(can_view = user).exists() or
                    dash.filter(owner_id = user).exists()):
                    result = backend_serializers.Orders_itemSerializer(dash[0]).data
                    result['editable'] = (dash.filter(owner_id = user).exists() or dash.filter(can_view = user). exists())
                    return_status = ststus.HTTP_200_OK
                else:
                    return_status = status.HTTP_403_FORBIDDEN

        else:
            return_status = status.HTTP_401_UNAUTHORIZED

        return JsonResponse(result, status=return_status, safe=False)


    def delete(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user

        result = None
        result_status = status.HTTP_404_NOT_FOUND

        if view_id is None:
            return JsonResponse(result, status=return_status, safe=False)

        if user.is_authenticated:
            dash = models.Orders_item.objects.filter(pk = view_id)
            if not dash.exists():
                return_status = status.HTTP_404_NOT_FOUND
            elif (dash.filter(can_edit = user).exists() or
                dash.filter(owner_id = user).exists()):
                result = backend_serializers.Orders_itemSerializer(dash[0]).data
                dash[0].delete()
                return_status = status.HTTP_200_OK
            else:
                return_status = status.HTTP_403_FORBIDDEN
        else:
            return_status = status.HTTP_403_FORBIDDEN

        return JsonResponse(result, status=return_status, safe=False)

   
    def put(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user
        result = None
        serializer = None
        return_status = status.HTTP_200_OK

        if user.is_authenticated:
            if view_id is not None:
                dash = models.Orders_item.objects.filter(pk = view_id)
                if not dash.exists():
                    return_status = status.HTTP_404_NOT_FOUND
                elif (dash.filter(can_edit = user).exists() or
                    dash.filter(owner_id = user).exists()):
                    dash = models.Orders_item.objects.filter(pk = view_id)
                    data = request.data
                    data['owner_id'] = dash[0].owner_id.pk;
                    serializer = backend_serializers.Orders_itemSerializer(dash[0], data=data)
                else:
                    print("Orders_item doesn't belong to the user")
                    print_status = status.HTTP_403_FORBIDDEN
            else:
                dash = models.Orders_item.objects.create(owner_id = user)
                data = request.data
                data['owner_id'] = user.pk
                serializer = backend_serializers.Orders_itemSerializer(dash, data=data)

            if serializer is not None:
                if serializer.is_valid():
                    serializer.save()
                    result = serializer.data
                    result['editable'] = True
                else:
                    result = {"errors": serializer.errors}
                    return_status = status.HTTP_400_BAD_REQUEST
        else:
            print("User is not authenticated");
            return_status = status.HTTP_403_FORBIDDEN

        return JsonResponse(result, status=return_status, safe=False)





















                


















