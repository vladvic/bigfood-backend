from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from bigfood import models
from bigfood import backend_serializers
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated


class ViewWaiters(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user
        result = None
        result_status = status.HTTP_404_NOT_FOUND

        if view_id is None:
            result = []
        else:
            return_status = status.HTTP_403_FORBIDDEN

        if user.is_authenticated:
            if view_id is None:
                owned = [dash for dash in models.Waiters.objects.filter(owner_id = user)]
                editable = [dash for dash in models.Waiters.objects.filter(can_edit = user)]
                viewable = [dash for dash in models.Waiters.objects.filter(can_view = user)]
                uniq = list(set(owned + editable + viewable))

                serializer = backend_serializers.WaitersSerializer()
                result =[backend_serializers.WaitersSerializer(dash).data for dash in uniq]
                if len(result) > 0:
                    result = list(map(lambda x: dict(**x, editable=((x['owner_id'] == user.pk) or (user.pk in x['can_view']))), result))
                    return_status = status.HTTP_200_OK
            else:
                dash = models.Waiters.objects.filter(pk = view_id)
                if not dash.exists():
                    return_status = status.HTTP_404_NOT_FOUND
                elif (dash.filter(can_edit = user).exists() or
                    dash.filter(can_view = user).exists() or
                    dash.filter(owner_id = user).exists()):
                    result = backend_serializers.WaitersSerializer(dash[0]).data
                    result['editable'] = (dash.filter(owner_id = user).exists() or dash.filter(can_view = user). exists())
                    return_status = ststus.HTTP_200_OK
                else:
                    return_status = status.HTTP_403_FORBIDDEN

        else:
            return_status = status.HTTP_401_UNAUTHORIZED

        return JsonResponse(result, status=return_status, safe=False)


    def delete(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user

        result = None
        result_status = status.HTTP_404_NOT_FOUND

        if view_id is None:
            return JsonResponse(result, status=return_status, safe=False)

        if user.is_authenticated:
            dash = models.Waiters.objects.filter(pk = view_id)
            if not dash.exists():
                return_status = status.HTTP_404_NOT_FOUND
            elif (dash.filter(can_edit = user).exists() or
                dash.filter(owner_id = user).exists()):
                result = backend_serializers.WaitersSerializer(dash[0]).data
                dash[0].delete()
                return_status = status.HTTP_200_OK
            else:
                return_status = status.HTTP_403_FORBIDDEN
        else:
            return_status = status.HTTP_403_FORBIDDEN

        return JsonResponse(result, status=return_status, safe=False)

   
    def put(self, request, format=None, **kwargs):
        view_id = kwargs.get("view_id", None)
        user = request.user
        result = None
        serializer = None
        return_status = status.HTTP_200_OK

        if user.is_authenticated:
            if view_id is not None:
                dash = models.Waiters.objects.filter(pk = view_id)
                if not dash.exists():
                    return_status = status.HTTP_404_NOT_FOUND
                elif (dash.filter(can_edit = user).exists() or
                    dash.filter(owner_id = user).exists()):
                    dash = models.Waiters.objects.filter(pk = view_id)
                    data = request.data
                    data['owner_id'] = dash[0].owner_id.pk;
                    serializer = backend_serializers.WaitersSerializer(dash[0], data=data)
                else:
                    print("Waiters doesn't belong to the user")
                    print_status = status.HTTP_403_FORBIDDEN
            else:
                dash = models.Waiters.objects.create(owner_id = user)
                data = request.data
                data['owner_id'] = user.pk
                serializer = backend_serializers.WaitersSerializer(dash, data=data)

            if serializer is not None:
                if serializer.is_valid():
                    serializer.save()
                    result = serializer.data
                    result['editable'] = True
                else:
                    result = {"errors": serializer.errors}
                    return_status = status.HTTP_400_BAD_REQUEST
        else:
            print("User is not authenticated");
            return_status = status.HTTP_403_FORBIDDEN

        return JsonResponse(result, status=return_status, safe=False)

