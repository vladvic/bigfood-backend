"""bigfood URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
#from django.conf.urls import url
from django.contrib import admin
from django.urls import path
from .super_views import ViewPlace
from .super_views import ViewMenu
from .super_views import ViewSections
from .super_views import ViewIngredients
from .super_views import ViewContents
from .super_views import ViewTables
from .super_views import ViewWaiters
from .super_views import ViewOrders
from .super_views import ViewCountry
from .super_views import ViewCity
from .super_views import ViewOrders_item
from rest_framework.authtoken.views import obtain_auth_token
#from main import views
#from rest_framework.authtoken.views import obtain_auth_token

urlpatterns = [
    #url(r'^admin/', admin.site.urls),
    path('api-token-auth/', obtain_auth_token),
    path('api/place/<view_id>',ViewPlace.as_view()),
    path('api/place/',ViewPlace.as_view()),
    path('api/menu/<view_id>',ViewMenu.as_view()),
    path('api/menu/',ViewMenu.as_view()),
    path('api/sections/<view_id>',ViewSections.as_view()),
    path('api/sections/',ViewSections.as_view()),
    path('api/ingredients/<view_id>',ViewIngredients.as_view()),
    path('api/ingredients/',ViewIngredients.as_view()),
    path('api/contents/<view_id>',ViewContents.as_view()),
    path('api/contents/',ViewContents.as_view()),
    path('api/tables/<view_id>',ViewTables.as_view()),
    path('api/tables/',ViewTables.as_view()),
    path('api/waiters/<view_id>',ViewWaiters.as_view()),
    path('api/waiters/',ViewWaiters.as_view()),
    path('api/orders/<view_id>',ViewOrders.as_view()),
    path('api/orders/',ViewOrders.as_view()),
    path('api/country/<view_id>',ViewCountry.as_view()),
    path('api/country/',ViewCountry.as_view()),
    path('api/city/<view_id>',ViewCity.as_view()),
    path('api/city/',ViewCity.as_view()),
    path('api/orders_item/<view_id>',ViewOrders_item.as_view()),
    path('api/orders_item/',ViewOrders_item.as_view()),
    path('admin/', admin.site.urls),
    #path('', views.chat)
    #path('api/auth/login', obtain_auth_token, name='api_token_auth'),  # <-- And here
    #path('api/auth/<operation>', AuthView.as_view()),
    
]

